import { View, Text } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from '../screens/HomeScreen'
import DestailScreen from '../screens/DestailScreen'
import TabsNavigator from './TabsNavigator'


const Stack = createStackNavigator()

const Navigator = () => {
  return (
    <Stack.Navigator>
        <Stack.Screen 
        name='TabsNavigator'
        component={TabsNavigator}
        options={{
            headerShown: null,
        }}
        />
        <Stack.Screen 
        name='DestailScreen'
        component={DestailScreen}
        />
    </Stack.Navigator>
  )
}

export default Navigator