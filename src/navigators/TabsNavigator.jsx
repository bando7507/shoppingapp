import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import HomeScreen from '../screens/HomeScreen'
import Inconic from '@expo/vector-icons/Ionicons'
import { MaterialIcons } from '@expo/vector-icons'; 




const Exemple = () => {
    return(
        <View>
            <Text>le</Text>
        </View>
    )
}

const TabsNavigator = () => {

    const TabsStack = createBottomTabNavigator()


  return (
    <TabsStack.Navigator
    screenOptions={{
        tabBarShowLabel: false
    }}
    >
        <TabsStack.Screen 
        options={{
            headerShown: null,
            tabBarIcon(props){
                return <Inconic name='home-outline' {...props}  />
            }
        }}
        name="Home" 
        component={HomeScreen} />
        <TabsStack.Screen 
        options={{
            headerShown: null,
            tabBarIcon(props){
                return<Inconic name='cart' {...props} />
            }
        }}
        name="Cart"
        component={Exemple} 
        />
        <TabsStack.Screen
        options={{
            headerShown: null,
            tabBarIcon(props){
                return <MaterialIcons name='payments' {...props} />
            }
        }}
        name="Payment" 
        component={Exemple} />

        <TabsStack.Screen
        options={{
            headerShown: null,
            tabBarIcon(props){
                return <MaterialIcons name='person' {...props} />
            }
        }}
        name="Profile"
        component={Exemple} />
    </TabsStack.Navigator>
  )
}

export default TabsNavigator