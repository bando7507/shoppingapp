import { View, Text, TouchableOpacity, TextInput, FlatList } from 'react-native'
import { StyleSheet } from 'react-native'
import React, { useCallback, useRef, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import { ScrollView } from 'react-native'
import { Image } from 'react-native'
import { useTheme } from '@react-navigation/native'
import { FontAwesome } from '@expo/vector-icons'; 
import { EvilIcons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 
import MasonryList from 'reanimated-masonry-list';
import { Feather } from '@expo/vector-icons'; 
import {BottomSheetModal, BottomSheetModalProvider,} from '@gorhom/bottom-sheet';


const AVATAR_URL = 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80'

const CATEGORIES = [
  'Clothing',
  'Shoes',
  'Accessories',
  'Accessories 2',
  'Accessories 3',
  'Accessories 4',
]



const HomeScreen = () => {

  const NewCollect = [
    {
      id: 1,
      imgLink: 'https://images.unsplash.com/photo-1638773867253-7fc8d950943b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=786&q=80',
      pice: 300
    },
    {
      id: 2,
      imgLink: 'https://images.unsplash.com/photo-1622253543934-c3cb280f8323?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80',
      pice: 130
    },
    {
      id: 3,
      imgLink: 'https://images.unsplash.com/photo-1659475820627-42388f0f5388?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
      pice: 280
    },
  ]


  const { colors } = useTheme()
  const [catId, setCatId] = useState('Clothing')
  // const bottomSheetModalRef = useRef<BottomSheetModal>(null);
  const bottomSheetModalRef = useRef(null);


  const OpenFilterModal = useCallback(() => {
    bottomSheetModalRef.current?.present();
  }, []);
  return (
    <ScrollView>
      <SafeAreaView style={{paddingVertical: 24}}>

        {/* Section Header */}
        <View style={{
          paddingHorizontal: 24,
          flexDirection: 'row',
          alignItems: 'center',
          gap: 8
        }}>
          <Image 
          source={{
            uri: AVATAR_URL
          }}
          resizeMode='cover'
          style={{
            width: 52,
            height: 52,
            borderRadius: 52,
          }}
          />
          <View style={{flex: 1}}>
            <Text style={{fontSize: 18, marginBottom:8 , fontWeight: "600"}} numberOfLines={1}>
              Hi, Abdel👋
            </Text>
            <Text style={{color: colors.text, opacity: 0.5}}>
              Discover fashion that suit your style
            </Text>
          </View>
          <TouchableOpacity style={{
            width: 52,
            height: 52,
            borderRadius: 52,
            borderWidth: 1,
            borderColor: colors.border,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <FontAwesome name="bell" size={24} color={colors.text} />
          </TouchableOpacity>
        </View>

        {/* Search Bar Section  */}
        <View style={{flexDirection: 'row', paddingHorizontal: 24, gap: 10, marginTop: 30}}>
          <View style={{
            flex: 1,
            height: 52,
            borderRadius: 52,
            borderWidth: 1,
            paddingHorizontal: 24,  
            flexDirection: 'row',
            alignItems: 'center'
          }}>
            <EvilIcons name="search" size={34} color={colors.text} />
            <TextInput 
            placeholder="Search"
            style={{
              flex: 1,
              height: "100%",
            }}
            />
          </View>

          <TouchableOpacity style={{
            width: 52,
            height: 52,
            borderRadius: 52,
            borderWidth: 1,
            alignItems: 'center',
            backgroundColor: colors.primary,
            justifyContent: 'center'
          }}
          onPress={OpenFilterModal}
          >
            <AntDesign name="filter" size={24} color={colors.border} />
          </TouchableOpacity>
        </View>

        {/* Grid Collection View */}
        <View style={{padding: 24,}}>
          {/* Title Bar */}
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}>
            <Text style={{fontSize: 24, fontWeight: '700'}}>New Collection</Text>
            <TouchableOpacity>
              <Text>See All</Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row', height: 200, gap: 12, borderRadius: 24, marginTop: 12}}>
            {/* Card */}
            <View style={{
              flex: 1
            }}>
                {NewCollect.slice(2, 3).map((item) => (
                <Card key={item.id} itemImg={item.imgLink} itemPrice={item.pice} />
              ))}
            </View>
            <View style={{flex: 1, gap: 12}}>
              {NewCollect.slice(0, 2).map((item) => (
                <Card key={item.id} itemImg={item.imgLink} itemPrice={item.pice} />
              ))}
            </View>
          </View>
        </View>

        {/* Categories Section */}
        <FlatList 
        data={CATEGORIES}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          gap:16,
          paddingHorizontal: 16
        }}
        keyExtractor={item => item}
        renderItem={({item}) => {
          const isSelect = catId === item
          return(
            <TouchableOpacity
            onPress={() => setCatId(item)}
            style={{
              backgroundColor: isSelect ? colors.primary : "#fff",
              paddingHorizontal: 24,
              borderWidth: isSelect ? 0 : 1,
              borderColor: colors.border,
              paddingVertical: 16,
              borderRadius: 100,
              marginBottom: 12
            }}>
              <Text 
              style={{
                color: isSelect ? colors.background : colors.text
              }}>{item}</Text>
            </TouchableOpacity>
          )
        }}
        />

        {/* Mesonary */}
        <MasonryList
          data={[1, 2, 3, 4, 34, 34,]}
          keyExtractor={(item) => item}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingHorizontal: 18,}}
          renderItem={({item, i}) => (
            <View style={{padding: 6}}>
              <View style={{
                aspectRatio: i === 0 ? 1 : 2/3,
                overflow: 'hidden',
                position: 'relative',
                // marginTop: 10,
                borderRadius: 24
              }}>
                <Image 
                source={{
                  uri: 'https://images.unsplash.com/photo-1681502014025-c14d1049416e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=388&q=80'
                }}
                style={StyleSheet.absoluteFill}
                resizeMode='cover'
                />

                <View style={StyleSheet.absoluteFill}>
                  <View style={{flexDirection: 'row', padding: 12, }}>
                    <Text style={{
                      flex: 1,
                      fontSize: 16,
                      fontWeight: '600',
                      color: colors.text
                      }}>
                      Puma Everyday Hussle 
                    </Text>
                    <View style={{
                      aspectRatio: 1,
                      borderRadius: 100,
                      backgroundColor: '#fff',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 32
                    }}>
                      <AntDesign name="hearto" size={20} color="black" />
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'flex-end', paddingHorizontal: 12}}>
                    <View style={{
                      flexDirection: 'row',
                      backgroundColor: 'rgba(0, 0, 0, 0.5)',
                      alignItems: 'center',
                      padding: 9,
                      borderRadius: 100,
                      marginBottom: 10
                    }}
                    intensity={20}
                    >
                      <Text style={{
                        flex: 1,
                        fontSize: 16,
                        fontWeight: '600',
                        color: "#fff"
                      }}>
                        130.00
                      </Text>
                      <TouchableOpacity style={{
                        backgroundColor: '#FFF',
                        paddingHorizontal: 12,
                        paddingVertical: 5,
                        borderRadius: 100,
                      }}>
                      <Feather name="shopping-bag" size={20} color="black" />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          )}
          onEndReachedThreshold={0.1}
        />
      </SafeAreaView>
      <BottomSheetModal
      ref={bottomSheetModalRef}
      snapPoints={['75%']}
      index={0}
      >
        <Text>Coucu</Text>
      </BottomSheetModal>
    </ScrollView>
  )
}

export default HomeScreen

const Card = ({itemImg, itemPrice}) => {
  return(
    <View style={{
      flex: 1,
      position: 'relative',
      overflow: 'hidden',
      borderRadius: 24
    }}>
      <Image 
      // source={{
      //   uri: 'https://images.unsplash.com/photo-1638773867253-7fc8d950943b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=786&q=80'
      // }}
      source={{
        uri: itemImg
      }}
      resizeMode='cover'
      style={{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        
      }}

      />
      <View style={{
        position: 'absolute',
        left: 16,
        top: 16,
        paddingHorizontal: 14,
        paddingVertical: 12,
        backgroundColor: 'rgba(0, 0, 0, 0.25)',
        borderRadius: 100
      }}>
        <Text style={{color: '#fff', fontSize: 14, fontWeight: '500'}}>
          ${itemPrice}
        </Text>
      </View>
    </View>
  )
}